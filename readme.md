# Flocking

Learning about Flocking from this tutorial: https://www.blog.drewcutchins.com/blog/2018-8-16-flocking

## To Run

Currently using [Parcel](https://parceljs.org/getting_started.html).

    npm install -g parcel-bundler
    parcel index.html

## Render/Update clock notes

RAF gives us a reliable clock for calling our render and update functions and accumulating time.

If we want the update or render functions to be called X times per second, we can define a number of milliseconds and wait until enough have been accumulated by RAF calls until we call the function.

# To Do
* Use decimal.js for numbers
