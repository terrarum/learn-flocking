const nowEl = document.querySelector('.js-now');
const updateEl = document.querySelector('.js-update');
const updateRateEl = document.querySelector('.js-updaterate');
const accumEl = document.querySelector('.js-accumulator');

let updatesCalled = 0;
let ops = 0;

// Time of previous loop.
let last = 0;
// Times Update is called every second
const updatesPerSecond = 60;
// MS between Update calls.
const updateRate = 1 / updatesPerSecond * 1000;
// MS since last Update call.
let timeAccumulator = 0;

updateRateEl.innerText = updateRate;

let state = null;
let update = null;
let render = null;
let ctx = null;

const frame = function frame(now) {
  // Time since last frame.
  let dt = now - last;
  timeAccumulator += dt;

  accumEl.innerText = timeAccumulator.toFixed(0);

  while(timeAccumulator > updateRate) {
    timeAccumulator -= updateRate;

    update(updateRate / 1000, state, ctx);

    updatesCalled += 1;
    updateEl.innerText = updatesCalled;
  }

  render(ctx, state);

  nowEl.innerText = now.toFixed(0);

  last = now;
  requestAnimationFrame(frame);
};

const init = function init(ctxP, stateP, updateP, renderP) {
  ctx = ctxP;
  state = stateP;
  update = updateP;
  render = renderP;

  requestAnimationFrame(frame);
};

export default {
  init,
};
