import gameloop from './src/controller/gameloop';

const dtEl = document.querySelector('.js-dt');
const opsEl = document.querySelector('.js-oncepersecond');

const vector = {
  x: 1,
  y: 1,
};

/**
 * Converts a polar coordinate to a vector component.
 *
 * @param magnitude - pixels
 * @param direction - degrees
 * @returns {{x: number, y: number}}
 */
const calculateCartesian = function calculateCartesian(magnitude, direction) {
  return {x: magnitude * Math.cos(direction), y: magnitude * Math.sin(direction)}
};

let angle = 0;
const updateCircle = function updateCircle(dt, state) {
  const length = state.circ.moveRate * dt;

  const vec = calculateCartesian(length, angle);

  state.circ.x += vec.x;
  state.circ.y += vec.y;

  angle += state.circ.turnRate * dt;
};

/**
 *
 * @param {number} dt - seconds
 * @param {object} state
 * @param {object} ctx
 */
const update = function update(dt, state, ctx) {
  dtEl.innerText = dt;
  const ops = state.ops + dt;
  state.ops = Math.round(ops * 100) / 100;

  const rect = state.rect;

  rect.x += rect.moveRate * dt;

  if (rect.x > ctx.canvas.width) {
    rect.x -= ctx.canvas.width;
  }

  updateCircle(dt, state);
};

const render = function render(ctx, state) {
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  opsEl.innerText = state.ops;

  ctx.fillRect(state.rect.x, state.rect.y, state.rect.size, state.rect.size);
  ctx.beginPath();
  ctx.arc(state.circ.x, state.circ.y, state.circ.radius, 0, Math.PI * 2);
  ctx.fill();
};

const init = function init() {
  const canvasEl = document.querySelector(".js-canvas");
  const ctx = canvasEl.getContext('2d');

  // Set the canvas size.
  ctx.canvas.width = 500;
  ctx.canvas.height = 500;

  const state = {
    ops: 0,
    rect: {
      x: 0,
      y: 200,
      size: 20,
      moveRate: 50, // pixels per second
    },
    circ: {
      x: 100,
      y: 100,
      radius: 10,
      moveRate: 50,
      turnRate: 1,
    }
  };

  gameloop.init(ctx, state, update, render);
};

init();
