// let now, dt = 0, last = 0, slow = 10, step = 1 / 60, slowStep = slow * step;
// let ctx, canvas;
//
// const render = function render(ctx) {
//   ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
// };
//
// const frame = function frame(now) {
//   dt = now - last;
//   while (dt > slowStep) {
//     dt = dt - slowStep;
//     update(step);
//   }
//   render(ctx);
//   last = now;
//   requestAnimationFrame(frame);
// };
//
// // Set up the loop.
// const init = function (updateP, renderP) {
//   // Get the canvas.
//   const canvasEl = document.querySelector(".js-canvas");
//   ctx = canvasEl.getContext("2d");
//   canvas = ctx.canvas;
//
//   // Set the canvas size.
//   canvas.width = 500;
//   canvas.height = 500;
//
//   // Start the loop loop.
//   requestAnimationFrame(frame);
// };

class Gameloop {
  constructor(update, render) {
    this.update = update;
    this.render = render;

    this.last = 0;
    this.updateRate = 1/60;
  }

  frame(timestamp) {
    // const e = document.querySelector('.t1');
    // e.innerHTML = this.state.x;
    let dt = timestamp - this.last;
    while (dt > this.updateRate) {
      dt -= this.updateRate;
      this.update(this.updateRate, this.state);
    }
    this.render(this.ctx, this.state);
    this.last = timestamp;
    requestAnimationFrame(this.frame.bind(this));
  }

  init(state) {
    const canvasEl = document.querySelector(".js-canvas");
    this.ctx = canvasEl.getContext("2d");
    this.state = state;

    // Set the canvas size.
    this.ctx.canvas.width = 500;
    this.ctx.canvas.height = 500;

    requestAnimationFrame(this.frame.bind(this));
  }
}

export default Gameloop;
