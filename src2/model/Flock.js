class Flock {
  constructor(flockSize) {
    this.boids = [];
    this.size = flockSize;
    this.populateFlock();
  }

  populateFlock() {
    for (let n = 0; n < this.size; n++) {

      //  The boids will be created at the center of the graph.
      this.boids.push(new Boid(0, 0));

      //  The angle of the boids are evenly distributed in a circle
      const angle = (n / this.size) * 2 * Math.PI;

      //  The velocity is set based on the calculated angle
      this.boids[n].velocity = {x: Math.cos(angle), y: Math.sin(angle)};
    }
  }

  updateFlock() {
    for (let i = 0; i < this.size; i++) {
      this.boids[i].update();
    }
  }
}

export default Flock;
