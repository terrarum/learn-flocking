import fpsIndicator from 'fps-indicator';

import Gameloop from './src/controller/Gameloop';
import Boid from './src/model/Boid';

fpsIndicator('top-right');

const update = function update(dt, state) {
  const e = document.querySelector('.t2');
  e.innerHTML = `update - ${state.rect.x}`;
  state.rect.x = state.rect.x + 1;
  window.dick = state;
};

const render = function render(ctx, state) {
  const e = document.querySelector('.t3');
  e.innerHTML = `render - ${state.rect.x}`;
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  ctx.fillRect(state.rect.x, state.rect.y, state.rect.size, state.rect.size);
};

const init = function init() {
  const gameloop = new Gameloop(update, render);

  const state = {
    rect: {
      size: 20,
      x: 0,
      y: 100,
    }
  };

  gameloop.init(state);
};

init();
